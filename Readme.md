<!-- <style>
pre {
  white-space: pre !important;
  overflow-y: scroll !important;
  max-height: 20vh !important;
}
</style> -->

# Herramienta para cargar y visualizar datos de movimiento del cuerpo humano.

Como parte de mi investigacion, he realizado algunos experimentos capturando el movimiento de todos los segmentos del cuerpo humano para luego poder analizarlos y visualizarlos. Para esto use camaras de captura de movimiento con marcadores puestos en el cuerpo (como estos https://www.youtube.com/watch?v=hZemcBhsMpw). El resultado fue procesado usando un modelo del cuerpo humano el cual genera la posicion de las diferentes articulaciones y como rotan respecto a los segmentos del cuerpo (por ejemplo, la posicion de la rodilla en X Y Z y el angulo entre la tibia y el femur, osea la rotacion en la rodilla). Estos datos ya generados estan guardados en un archivo .mat de matlab el cual contiene una estructura con los siguientes campos:

- Positions, Accelerations, Velocities, Model_Marker_Errors_from_IK, Model_Marker_Locations_from_IK

Estamos interesados en los 3 primeros campos (Positions, Accelerations, Velocities). Cada uno de estos campos contiene señales que describen la posicion y orientacion de los segmentos del cuerpo ademas de los angulos de las articulaciones. La lista completa es:



Tiempo:

    {'time'            }

Posiciones y Orientaciones:



    {'pelvis_X'        }
    {'pelvis_Y'        }
    {'pelvis_Z'        }
    {'pelvis_Ox'       }
    {'pelvis_Oy'       }
    {'pelvis_Oz'       }
    {'femur_r_X'       }
    {'femur_r_Y'       }
    {'femur_r_Z'       }
    {'femur_r_Ox'      }
    {'femur_r_Oy'      }
    {'femur_r_Oz'      }
    {'tibia_r_X'       }
    {'tibia_r_Y'       }
    {'tibia_r_Z'       }
    {'tibia_r_Ox'      }
    {'tibia_r_Oy'      }
    {'tibia_r_Oz'      }
    {'talus_r_X'       }
    {'talus_r_Y'       }
    {'talus_r_Z'       }
    {'talus_r_Ox'      }
    {'talus_r_Oy'      }
    {'talus_r_Oz'      }
    {'calcn_r_X'       }
    {'calcn_r_Y'       }
    {'calcn_r_Z'       }
    {'calcn_r_Ox'      }
    {'calcn_r_Oy'      }
    {'calcn_r_Oz'      }
    {'toes_r_X'        }
    {'toes_r_Y'        }
    {'toes_r_Z'        }
    {'toes_r_Ox'       }
    {'toes_r_Oy'       }
    {'toes_r_Oz'       }
    {'femur_l_X'       }
    {'femur_l_Y'       }
    {'femur_l_Z'       }
    {'femur_l_Ox'      }
    {'femur_l_Oy'      }
    {'femur_l_Oz'      }
    {'tibia_l_X'       }
    {'tibia_l_Y'       }
    {'tibia_l_Z'       }
    {'tibia_l_Ox'      }
    {'tibia_l_Oy'      }
    {'tibia_l_Oz'      }
    {'talus_l_X'       }
    {'talus_l_Y'       }
    {'talus_l_Z'       }
    {'talus_l_Ox'      }
    {'talus_l_Oy'      }
    {'talus_l_Oz'      }
    {'calcn_l_X'       }
    {'calcn_l_Y'       }
    {'calcn_l_Z'       }
    {'calcn_l_Ox'      }
    {'calcn_l_Oy'      }
    {'calcn_l_Oz'      }
    {'toes_l_X'        }
    {'toes_l_Y'        }
    {'toes_l_Z'        }
    {'toes_l_Ox'       }
    {'toes_l_Oy'       }
    {'toes_l_Oz'       }
    {'torso_X'         }
    {'torso_Y'         }
    {'torso_Z'         }
    {'torso_Ox'        }
    {'torso_Oy'        }
    {'torso_Oz'        }
    {'center_of_mass_X'}
    {'center_of_mass_Y'}
    {'center_of_mass_Z'}
    
    
Angulos de rotacion 
    
    {'pelvis_tilt'     }
    {'pelvis_list'     }
    {'pelvis_rotation' }
    {'pelvis_tx'       }
    {'pelvis_ty'       }
    {'pelvis_tz'       }
    {'hip_flexion_r'   }
    {'hip_adduction_r' }
    {'hip_rotation_r'  }
    {'knee_angle_r'    }
    {'ankle_angle_r'   }
    {'subtalar_angle_r'}
    {'mtp_angle_r'     }
    {'hip_flexion_l'   }
    {'hip_adduction_l' }
    {'hip_rotation_l'  }
    {'knee_angle_l'    }
    {'ankle_angle_l'   }
    {'subtalar_angle_l'}
    {'mtp_angle_l'     }
    {'lumbar_extension'}
    {'lumbar_bending'  }
    {'lumbar_rotation' }

Por ejemplo `'pelvis_X', 'pelvis_Y' y 'pelvis_Z'` son vectores que contienen la posicion X Y Z de la pelvis. `'pelvis_Ox'`, `'pelvis_Oy'` y `'pelvis_Oz'` son vectores que contienen la orientación de la pelvis en angulos de euler (Por lo pronto estos angulos no son importantes). Ademas de estos campos existen los campos 
que contienen los angulos de rotaciones de las articulaciones (por ejemplo knee_angle_r es el angulo de flexion o extension de la rodilla derecha)

Cada campo de Positions, Accelerations, Velocities contiene tambien un vector de tiempo (time) para todas las otras señales. Por ejemplo para ver como la rodilla izquierda se flexiona a traves del tiempo se podria hacer una grafica de `time` vs `knee_angle_l`, o si se quiere ver la posicion en el eje X de la pelvis a traves del tiempo se puede graficar `time` vs `pelvis_X`.

Como estas los datos estan guardados en el contenedor .mat de matlab es facil cargarlos en matlab y hacer estas graficas. Por ejemplo usando el siguiente codigo en matlab

```matlab 
load Data.mat
plot(Positions.time,Positions.knee_angle_r)
ylabel('Right Knee angle (deg)')
xlabel('Time (s)')
```

Sin embargo la idea es poder realizar la visulizacion en Python.

El proyecto consiste entonces en hacer una herramienta en python de varios scripts que sirvan para cargar y visualizar datos de movimiento de los experimentos con humanos.

Partes:

1. Crear una funcion `load_data` que use como argumento el nombre del archivo .mat, cargue los datos y devuelva los datos de los siguientes campos de la estructura:
    - Positions, Accelerations, Velocities y que cada uno de estos campos contenga todas las señales descritas anteriormente (Tiempo, Posiciones y Orientaciones, Angulos de rotacion). 
    Por ejemplo la funcion
    ```python
    my_data = load_data('Positions','Data.mat')
    ```
    deberia cargar las señales referentes a la posicion del archivo "Data.mat" en la variable "my_data"

2. Crear una funcion `plot_time_series` que sirva para graficar las señales en el tiempo que tenga como argumentos:
    - Alguno de los campos Positions, Velocities, Accelerations, cargados por la funcion `load_data` 
    - Un string que sirva para introducir el nombre de la señal a graficar

    La funcion debe relizar graficas de las 3 componentes de la posicion en graficas separadas (por ejemplo una figura con 3 graficas una encima de la otra con los valores X,Y y Z)

    Por ejemplo la funcion
    ```python
    plot_time_series(my_data,'pelvis')
    ```
    deberia mostrar una ventana con 3 graficas correspondientes a 'pelvis_X', 'pelvis_Y' y 'pelvis_Z'


3. Crear una funcion `plot_trajectory` que permita visualizar la trayectoria en 2D (En vista frontal, superior y lateral) de alguno de los segmentos del cuerpo. La funcion debe tener como argumentos:
    - El campo Positions de la funcion 1
    - El nombre del segmento (por ejemplo 'pelvis')

    Por ejemplo el comando
    ```python
    plot_trajectory(position_data,'pelvis')
    ```
    deberia graficar una linea de longitud 1 con punto medio en "pelvis_X","pelvis_Y","pelvis_Z" y con orientacion "pelvis_Ox","pelvis_Oy","pelvis_Oz"

    La funcion debe crear una figura con 3 vistas (frontal, superior y lateral) donde se grafiquen la posicion de los segmentos (por ejemplo para si el argumento de entrada es el string `'pelvis'` la grafica debera usar las señales `pelvis_X`, `pelvis_Y`, `pelvis_Z` para la posicion y `pelvis_Ox`, `pelvis_Oy`, `pelvis_Oz` para la orientacion.

4. Crear una funcion `animate_avatar` en la que se anime el movimiento de los segmentos del cuerpo. Los argumentos de esta funcion son:
    - El campo Positions de la funcion de la parte 1

    La funcion debe crear una grafica que muestre a forma de animacion como se mueve el cuerpo completo. Para esta funcion basta con usar lineas como segmentos del cuerpo.
